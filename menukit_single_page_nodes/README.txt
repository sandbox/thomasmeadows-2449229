CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Recommended Modules
* Recommended JQuery
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
  MenuKit Single Page Nodes allows you to turn menus into single
  page websites and have pages preloaded and run with JQuery
  animations.

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2449229

REQUIREMENTS
------------

This module requires the following modules:

* MenuKit (included)
* SimpleHTMLDom (https://www.drupal.org/project/simplehtmldom)

RECOMMENDED MODULES
-------------------
* Display Suite(https://www.drupal.org/project/displaysuite)
* Quick Tabs(https://www.drupal.org/project/quicktabs)

RECOMMENDED JQUERY
------------------

* JQuery Mousewheel
  install @ sites/all/libraries/jquery.mousewheel/jquery.mousewheel.js
  https://github.com/jquery/jquery-mousewheel

* JQuery Easing
  install @ sites/all/libraries/jquery.easing/jquery.easing.js 
  https://github.com/gdsmith/jquery.easing

CONFIGURATION
-------------

* www.example.com/node/add/single-page-nodes

MAINTAINERS
-----------
Current maintainers:
* Thomas J. Meadows  - https://www.drupal.org/u/thomasmeadows

This project is unsponsered

