/**
 * @file
 * System for creating single page nodes.
 */

(function($) {
  $(document).ready(function() {
    var isscrolling = false;
    var menuname = "#" + Drupal.settings.menukit_single_page_nodes.menuname;
    var easing = Drupal.settings.menukit_single_page_nodes.easing;
    var duration = + Drupal.settings.menukit_single_page_nodes.duration;
    var horizontal = + Drupal.settings.menukit_single_page_nodes.horizontal;
    var mouse_wheel = + Drupal.settings.menukit_single_page_nodes.mouse_wheel;
    var horizontalmousescroll = + Drupal.settings.menukit_single_page_nodes.horizontalmousescroll;
    var anchorscroll = + Drupal.settings.menukit_single_page_nodes.anchorscroll;
    var first = 0;
    var menuarray = new Array();
    $(menuname + " a").each(function(item) {
      menuarray.push(this.pathname);
    });
    function notscrolling() {
      window.setTimeout(function() {isscrolling = false;change_history();}, duration - 50);
    }
    $(".single_page_wrapper").css("clear", "both");
    $(".single_page_wrapper .single_page").css("overflow", "inherit");
    $(menuname + " li:first").addClass("active");
    $(menuname + " li:first a").addClass("active");
    var target_offset = $(".single_page_wrapper:first").offset();
    var target_top = target_offset;
    var isscrolling = true;
    if (horizontal) {
      $('html, body').animate({
        scrollLeft:target_top.left
      }, {
        duration: duration,
        easing: easing,
        complete: notscrolling()
      });
    }
    else {
      $('html, body').animate({
        scrollTop:target_top.top
      }, {
        duration: duration,
        easing: easing,
        complete: notscrolling()
      });
    }
    var anchor = location.hash;
    if (anchor != "") {
      var current_item = $(anchor);
      var index = $("div.single_page_wrapper").index(current_item);
      $(menuname + " li").removeClass("active");
      $(menuname + " li a").removeClass("active");
      $(menuname + " li:eq(" + index + ")").addClass("active");
      $(menuname + " li:eq(" + index + ") a").addClass("active");
      var target_top = $(anchor).offset();
      isscrolling = true;
      if (horizontal) {
        $('html, body').animate({
          scrollLeft:target_top.left
        }, {
          duration: duration,
          easing: easing,
          complete: notscrolling()
        });
      }
      else {
        $('html, body').animate({
          scrollTop:target_top.top
        }, {
          duration: duration,
          easing: easing,
          complete: notscrolling()
        });
      }
    }
    var basePath = Drupal.settings.basePath;
    $(menuname + " a").click(function(event) {
      $(menuname + " li").removeClass("active");
      $(menuname + " li a").removeClass("active");
      $(this).addClass("active");
      $(this).parent().addClass("active");
      // Prevent the default action for the click event.
      event.preventDefault();
      var anchor = this.pathname.replace(basePath, '');
      var trgt = anchor.replace("/","---");
      // Get the top offset of the target anchor.
      var target_top = $("#" + trgt).offset();
      // Goto that anchor by setting the body scroll top to anchor top.
      isscrolling = true;
      if (horizontal) {
        $('html, body').animate({
          scrollLeft:target_top.left
        }, {
          duration: duration,
          easing: easing,
          complete: notscrolling()
        });
      }
      else {
        $('html, body').animate({
          scrollTop:target_top.top
        }, {
          duration: duration,
          easing: easing,
          complete: notscrolling()
        });
      }
    });
    $(window).scroll(function(event) {
      function change_history() {
        $(".single_page_wrapper").each(function(item) {
          if (horizontal) {
            // If user scrolling in region 1/2 the area and scrolling horizontal.
            if (window.pageXOffset >= ($("#" + this.id).offset().left - 50) && window.pageXOffset <= ($("#" + this.id).offset().left + ($("#" + this.id).width()) / 2)) {
              if (this.id != window.location.pathname.substring(1) && this.id != "/") {
                var basePath = Drupal.settings.basePath;
                var newanchor = this.id.replace("---","/");
                if (window.location.pathname != (basePath + newanchor)) {
                  // Set Timeout counter to prevent duplicate adds to history.
                  first = first + 1;
                  if (first == 1) {
                    history.pushState(null, null,basePath + newanchor);
                    $(menuname + " li").removeClass("active");
                    $(menuname + " li a").removeClass("active");
                    // Make menu item active that user scrolling over.
                    $(menuname + " a").each(function(item) {
                      if (this.href == window.location) {
                        $(this).addClass("active");
                        $(this).parent().addClass("active");
                      }
                    });
                  }
                  // Set timeout counter to prevent duplicate adds to history.
                  window.setTimeout(function() {first = 0;}, (100))
                }
              }
            }
          }
          else {
            // If user scrolling in region 1/2 the area and scrolling vertical.
            if (window.pageYOffset >= ($("#" + this.id).offset().top - 50) && window.pageYOffset <= ($("#" + this.id).offset().top + ($("#" + this.id).height()) / 2)) {
              if (this.id != window.location.pathname.substring(1) && this.id != "/") {
                var basePath = Drupal.settings.basePath;
                var newanchor = this.id.replace("---","/");
                if (window.location.pathname != (basePath + newanchor)) {
                  // Set timeout counter to prevent duplicate adds to history.
                  first = first + 1;
                  if (first == 1) {
                    history.pushState(null, null,basePath + newanchor);
                    $(menuname + " li").removeClass("active");
                    $(menuname + " li a").removeClass("active");
                    // Make menu item active that user scrolling over.
                    $(menuname + " a").each(function(item) {
                      if (this.href == window.location) {
                        $(this).addClass("active");
                        $(this).parent().addClass("active");
                      }
                    })
                  }
                  // Set Timeout counter to prevent duplicate adds to history.
                  window.setTimeout(function() {first = 0;}, (100))
                }
              }
            }
          }
        });
      }
      if (isscrolling == false) {
        change_history();
      }
    });
    $(window).bind('popstate', function (event) {
      var trgt = window.location.pathname.replace(basePath, '').replace("/","---");
      var target_top = $("#" + trgt).offset();
      isscrolling = true;
      if (horizontal) {
        $('html, body').animate({
          scrollLeft:target_top.left
        }, {
          duration: duration,
          easing: easing,
          complete: notscrolling()
        });
      }
      else {
        $('html, body').animate({
          scrollTop:target_top.top
        }, {
          duration: duration,
          easing: easing,
          complete: notscrolling()
        });
      }
    });
    // If mousewheel.js is enabled.
    if (mouse_wheel) {
      // If anchorscroll.js is enabled.
      if (anchorscroll) {
        $('html, body, *').mousewheel(function(e, deltaX, deltaY) {
          var mCount = 0;
          $(menuname + " a").each(function(item) {
            if (menuarray[mCount] == window.location.pathname) {
              e.preventDefault();
              if (isscrolling) {return;}
              var menunum;
              if (deltaY) {
                if (deltaY < 0) {
                  menunum = mCount + 1;
                  if (menuarray[menunum].length == 0) {menunum--;}
                }
                else if (deltaY > 0) {
                  menunum = mCount - 1;
                  if (menuarray[menunum].length == 0) {menunum++;}
                }
              }
              else if (deltaX) {
                if (deltaX < 0) {
                  menunum = mCount + 1;
                  if (menuarray[menunum].length == 0) {menunum--;}
                }
                else if (deltaX > 0) {
                  menunum = mCount - 1;
                  if (menuarray[menunum].length == 0) {menunum++;}
                }
                var anchortarget = menuarray[menunum].replace(basePath, '').replace("/","---");
                isscrolling = true;
                if (horizontalmousescroll) {
                  $('html, body').animate({
                    scrollLeft:$("#" + anchortarget).offset().left
                  }, {
                    duration: duration,
                    easing: easing,
                    complete: notscrolling()
                  });
                }
                else {
                  $('html, body').animate({
                    scrollTop:$("#" + anchortarget).offset().top
                  }, {
                    duration: duration,
                    easing: easing,
                    complete: notscrolling()
                  });
                }
              }
            };
            mCount++;
          });
        });
      }
      // Anchor.js disabled and mousewheel is enabled.
      else {
        if (horizontalmousescroll) {
          $('html, body, *').mousewheel(function(e, deltaX, deltaY) {
            this.scrollLeft -= (deltaX * 40);
            e.preventDefault();
          });
        }
      }
    }
  });
})(jQuery);
