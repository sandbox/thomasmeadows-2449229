CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
  Main menu kit module allows you AJAX all of your
  menus in drupal for a faster loading experience.

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2449229

REQUIREMENTS
------------

This module requires the following modules:

* Libraries (https://www.drupal.org/project/libraries)
   
CONFIGURATION
-------------

* www.example.com/structure/menu/local_task -or-
* www.example.com/structure/menu and then click edit menu option 
  next to the menu you want to ajax


MAINTAINERS
-----------
Current maintainers:
* Thomas J. Meadows  - https://www.drupal.org/u/thomasmeadows

This project is unsponsered
