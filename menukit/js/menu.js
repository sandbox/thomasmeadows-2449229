/**
 * @file
 * Menu system for Ajax'ing menus.
 */

(function($, Drupal) {
  $(document).ready(function() {
    var fullpagerefresh = false;
    var headbodyrefresh = false;
    function head_body_refresh($urlindex, $menu_name, $ajax_html) {
      $.each(Drupal.settings.menukit_ajax_headbody_refresh, function(index, value) {
        if ($menu_name == index) {
          var found = $("#columns", $ajax_html);
          // Rotate through all partialrefresh options set'.
          $.each(found, function(k, v) {
            $("#columns").replaceWith(v);
          });
          if (found.length <= 0) {
            var found = $("#content", $ajax_html);
            // Rotate through all partialrefresh options set'.
            $.each(found, function(k, v) {
              $("#content").replaceWith(v);
            });
          }

          var headfind = $ajax_html.match(/<head>[\s\S]*<\/head>/gi);
          $.each(headfind, function(i, v) {
            if (v.length > 0) {
              v = v.replace(/<head>/gi, '');
              v = v.replace(/<\/head>/gi, '');
              document.head.innerHTML = v;
            }
          });
          history.pushState(null, null, Drupal.settings.basePath + Drupal.settings.pathPrefix + $urlindex);
          headbodyrefresh = true;
        }
      });
    };
    function full_page_refresh($urlindex, $menu_name, $ajax_html) {
      $.each(Drupal.settings.menukit_ajax_full_refresh, function(index, value) {
        if ($menu_name == index) {
          document.open();
          document.write($ajax_html);
          document.close();
          history.pushState(null, null, Drupal.settings.basePath + Drupal.settings.pathPrefix + $urlindex);
          fullpagerefresh = true;
        }
      });
    };
    function partial_page_refresh($urlindex, $menu_name, $ajax_html) {
      // If menu is a local task defined by /structure/menu/local_task.
      if ($menu_name == "local_task") {
        var CheckBoxes = Drupal.settings.menukit_ajax_check_boxes;
        $.each(CheckBoxes, function(index, value) {
          var found = $("#" + value, $ajax_html);
          // Rotate through all partialrefresh options set.
          $.each(found, function(key, val) {
            $("#" + value).replaceWith(val);
          });
        });
        history.pushState(null, null, Drupal.settings.basePath + Drupal.settings.pathPrefix + $urlindex);
        var headfind = $ajax_html.match(/<head>[\s\S]*<\/head>/gi);
        $.each(headfind, function(i, v) {
          if (v.length > 0) {
            v = v.replace(/<head>/gi, '');
            v = v.replace(/<\/head>/gi, '');
            document.head.innerHTML = v;
          }
        });
      }
      else {
        // Else, menu is a main menu defined in drupal structure/menus.
        var MenuCheckBoxes = Drupal.settings.menukit_ajax_check_boxes_menuname;
        // Get strings under menu name.
        $.each(MenuCheckBoxes,function(ind_x, ind_y) {
          $.each(ind_y, function(index, value) {
            var found = $("#" + value, $ajax_html);
            // Rotate through all partialrefresh options set.
            $.each(found, function(key, val) {
              $("#" + value).replaceWith(val);
              if (value.indexOf("_")) {
                value.replaceAll(/_/g , "-")
                $("#" + value).replaceWith(val);
              }
            });
          });
        });
        if ($urlindex != "<front>") {
          history.pushState(null, null, Drupal.settings.basePath + Drupal.settings.pathPrefix + $urlindex);
        }
        else {
          history.pushState(null, null, Drupal.settings.basePath + Drupal.settings.pathPrefix);
        }
        var headfind = $ajax_html.match(/<head>[\s\S]*<\/head>/gi);
        $.each(headfind, function(i, v) {
          if (v.length > 0) {
            v = v.replace(/<head>/gi, '');
            v = v.replace(/<\/head>/gi, '');
            document.head.innerHTML = v;
          }
        });
      }
    };
    // Roll through each variable imported from php.
    if (Drupal.settings.menukit_exception) {
      return;
    }
    $.each(Drupal.settings.menukit_ajax, function(index, value) {
      if (index != '<front>') {
        // If link does not go to front page.
        $('a[href$="' + index + '"]').click(function(event) {
          $(".active").removeClass("active");
          // Reload entire page if going from admin to front page or vice versa.
          if (window.location.pathname.indexOf("/admin") >= 0 && index.indexOf("admin") < 0) {
            return;
          }
          if (window.location.pathname.indexOf("/admin") < 0 && index.indexOf("admin") >= 0) {
            return;
          }
          event.preventDefault();
          $('a[href$="' + index + '"]').addClass("active");
          $('a[href$="' + index + '"]').parent('li').addClass("active");
          $.get(Drupal.settings.menukit.base_url + '/' + index, function(data) {
            // Full page refresh if set in drupal.
            head_body_refresh(index, value, data);
            // Probably unnecessary, but returns on full page refresh.
            if (headbodyrefresh){
              return;
            }
            // Full page refresh if set in drupal.
            full_page_refresh(value, data);
            // Probably unnecessary, but returns on full page refresh.
            if (fullpagerefresh){
              return;
            }
            partial_page_refresh(index, value, data);
            return;
          });
        });
      }
      else {
        // If link goes to the front page.
        $('a[href$="' + Drupal.settings.basePath + '"]').click(function(event) {
          $(".active").removeClass("active");
          // Reload entire page if going from admin to front page or vice versa.
          if (window.location.pathname.indexOf("/admin") >= 0 && index.indexOf("admin") < 0) {
            return;
          }
          if (window.location.pathname.indexOf("/admin") < 0 && index.indexOf("admin") >= 0) {
            return;
          }
          event.preventDefault();
          $('a[href$="' + index + '"]').addClass("active");
          $('a[href$="' + index + '"]').parent('li').addClass("active");
          $.get(Drupal.settings.base_url, function(data) {
            // Full page refresh if set in drupal.
            head_body_refresh(index, value, data);
            // Probably unnecessary, but returns on full page refresh.
            if (headbodyrefresh){
              return;
            }
            // Full page refresh if set in drupal.
            full_page_refresh(value, data);
            // Probably unnecessary, but returns on full page refresh.
            if (fullpagerefresh){
              return;
            }
            partial_page_refresh(index, value, data);
            return;
          });
        });
      }
    });
  });
  // If window path is for the overlay module, don't pop.
  if (window.location.pathname.indexOf("#overlay") >= 0) {
    // Define pop state for when the back button is pressed.
    $(window).bind('popstate', function (event) {
      $.get('/' + window.location.pathname.substring(1), function(data) {
        window.location.replace(window.location.pathname);
      });
    });
  }
})(jQuery, Drupal);
